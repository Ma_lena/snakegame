// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "RandomItem.generated.h"

UCLASS()
class SNAKEGAME_API ARandomItem : public AActor
{
	GENERATED_BODY()
	
public:	
		ARandomItem();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Spawn Items")
		TArray<TSubclassOf<AActor>>RandomItemPool;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Components")
		UBillboardComponent* BillboardComponent;
protected:
		virtual void BeginPlay() override;

private:
	UPROPERTY()
		UChildActorComponent* ChildActorComponent;


};
