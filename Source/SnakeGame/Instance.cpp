// Fill out your copyright notice in the Description page of Project Settings.


#include "Instance.h"
#include "Blueprint/UserWidget.h"
#include "UObject/ConstructorHelpers.h"

UInstance::UInstance(const FObjectInitializer& objectinitializer)
{
	static ConstructorHelpers::FClassFinder<UUserWidget>MainMenuWidget(TEXT("/Game/MainMenu"));
	if (!ensure (MainMenuWidget.Class != nullptr)) return; // �������� �� ������� ���������

	MainMenuWidgetClass = MainMenuWidget.Class;

}

void UInstance::Init()
{
	UE_LOG(LogTemp, Warning, TEXT("We are founded class%s"), *MainMenuWidgetClass->GetName());
}

void UInstance::ShowWidget()
{
	UUserWidget* MainMenu = CreateWidget<UUserWidget>(this, MainMenuWidgetClass); // ������� ������
	MainMenu->AddToViewport();

	APlayerController* PlayerController = GetFirstLocalPlayerController(); // ������� ��� �� �����

	FInputModeUIOnly InputModeDate;// ����� ���������
	InputModeDate.SetWidgetToFocus(MainMenu->TakeWidget());
	InputModeDate.SetLockMouseToViewportBehavior(EMouseLockMode::DoNotLock);

	PlayerController->SetInputMode(InputModeDate);

	PlayerController->bShowMouseCursor = true;
}
