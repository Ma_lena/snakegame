// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "Instance.generated.h"

/**
 * 
 */
UCLASS()
class SNAKEGAME_API UInstance : public UGameInstance
{
	GENERATED_BODY()

public:
	UInstance(const FObjectInitializer& objectinitializer);

	virtual void Init();

	UFUNCTION(BlueprintCallable)
	void ShowWidget();

	

private:
	TSubclassOf<class UUserWidget> MainMenuWidgetClass; // ���������� ��� ������ �������

	
};
