// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "interactable.h"
#include "Instance.h"


// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 100.f;
	MovementSpeed = 10.f;
	LastMoveDirection = EMovementDirection::DOWN;
	LastElement = 0;
	Score = 0;
	//PitchValue = 0.f;
	//YawValue = 0.f;
	//RollValue = 0.f;
}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickInterval(MovementSpeed);
	FirstSnakeElement(5);
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();
	//UE_LOG(LogTemp, Warning, TEXT("Eat Food: %d"), Score);
}


void ASnakeBase::FirstSnakeElement(int ElementsNum)
{
	for (int i = 0; i < ElementsNum; ++i)
	{
		FVector NewLocation(SnakeElements.Num() * ElementSize, 0, 0);
		FTransform NewTransform(NewLocation);
		ASnakeElementBase* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
		NewSnakeElem->SnakeOwner = this;
		LastElement = SnakeElements.Add(NewSnakeElem);
		if (LastElement == 0)
		{
			NewSnakeElem->SetFirstElementType();
			
		}

	}
}



void ASnakeBase::Move()
{
	FVector MovementVector(ForceInitToZero);
		
	switch (LastMoveDirection)
	{
	case EMovementDirection::UP:
			MovementVector.X += ElementSize;
			break;
	case EMovementDirection::DOWN:
			MovementVector.X -= ElementSize;
			break;
	case EMovementDirection::LEFT:
			MovementVector.Y += ElementSize;
			break;
	case EMovementDirection::RIGHT:
			MovementVector.Y -= ElementSize;
			break;
	}

	//AddActorWorldOffset (MovementVector);
	SnakeElements[0]->ToggleCollision();

	for(int i = SnakeElements.Num() - 1; i>0; i--)
	{
		auto CurrentElement = SnakeElements[i];
		auto PrevElement = SnakeElements[i - 1];
		FVector PrevLocation = PrevElement->GetActorLocation();
		CurrentElement->SetActorLocation(PrevLocation);
	}

	SnakeElements[0]->AddActorWorldOffset(MovementVector);
	SnakeElements[0]->ToggleCollision();
}

void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other)
{
	if (IsValid(OverlappedElement))
	{
		int32 ElemIndex;
		SnakeElements.Find(OverlappedElement, ElemIndex);
		bool bIsFirst = ElemIndex == 0;
		IInteractable* InteractableInterface = Cast<IInteractable>(Other);
		if (InteractableInterface)
		{
			InteractableInterface->Interact(this, bIsFirst);
		}
	}
}

void ASnakeBase::AddSnakeElement(int ElementsNum)
{
	for (int i = 0; i < ElementsNum; ++i)
	{
		auto CurrentElement = SnakeElements[LastElement];
		FVector CurrentLocation = CurrentElement->MeshComponent->GetComponentLocation();
		FVector NewLocation(CurrentLocation.X, CurrentLocation.Y, CurrentLocation.Z);
		FTransform NewTransform(NewLocation);
		ASnakeElementBase* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
		NewSnakeElem->SnakeOwner = this;
		LastElement = SnakeElements.Add(NewSnakeElem);
		
	}
}

int ASnakeBase::SetScore()
{
	Score += 1;
	return 0;
}

int ASnakeBase::GetScore()
{
	return Score;
}

/*void ASnakeBase::Rotat()
{
	FQuat QuatRotation =FQuat( FRotator(PitchValue, YawValue, RollValue));
	AddActorLocalRotation(QuatRotation, false, 0, ETeleportType::None);
}*/




