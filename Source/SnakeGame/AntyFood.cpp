// Fill out your copyright notice in the Description page of Project Settings.


#include "AntyFood.h"
#include "SnakeBase.h"

// Sets default values
AAntyFood::AAntyFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	
}

// Called when the game starts or when spawned
void AAntyFood::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AAntyFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AAntyFood::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->
			Destroy();
		}
	}
}

