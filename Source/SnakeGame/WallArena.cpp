// Fill out your copyright notice in the Description page of Project Settings.


#include "WallArena.h"
#include "SnakeBase.h"
#include "Instance.h"

// Sets default values
AWallArena::AWallArena()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AWallArena::BeginPlay()
{
	Super::BeginPlay();
	
}




// Called every frame
void AWallArena::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}




void AWallArena::Interact(AActor* Interactor, bool bIsHead)
{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->Destroy();
		}
	
}
