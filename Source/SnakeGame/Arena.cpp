// Fill out your copyright notice in the Description page of Project Settings.


#include "Arena.h"


// Sets default values
AArena::AArena()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	Floor = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("FloorComponent"));
	SetRootComponent(Floor);



}

// Called when the game starts or when spawned
void AArena::BeginPlay()
{
	Super::BeginPlay();

	FTimerHandle Handle;
	GetWorld()->GetTimerManager().SetTimer(Handle, this, &AArena::Tim, 5.f, true);
	
	
}

// Called every frame
void AArena::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
	
	
}

void AArena::SpawnFood(UClass* FoodToSpawn)
{
	float XCoorDinate = FMath::FRandRange(-400.f, 400.f);
	float YCoorDinate = FMath::FRandRange(-400.f, 400.f);

	

	FVector Location(XCoorDinate, YCoorDinate, 0.f);
	FRotator Rotator(0.f, 0.f, 0.f);
	
	GetWorld()->SpawnActor<AFood>(FoodToSpawn, Location, FRotator(0.f));
}

void AArena::SpawnWall(UClass* WallToSpawn)
{
	float XCoorDinate = FMath::FRandRange(-400.f, 400.f);
	float YCoorDinate = FMath::FRandRange(-400.f, 400.f);
	
	

	FVector Location(XCoorDinate, YCoorDinate, 0.f);
	FRotator  Rotation(0.f, 0.f, 0.f);

	GetWorld()->SpawnActor<AWall>(WallToSpawn, Location, Rotation);
	
}

void AArena::Tim()
{
	SpawnFood(Food);
	SpawnWall(Wall);
	
	
}
