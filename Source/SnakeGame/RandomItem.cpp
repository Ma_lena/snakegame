// Fill out your copyright notice in the Description page of Project Settings.


#include "RandomItem.h"
#include "Components/BillboardComponent.h"

ARandomItem::ARandomItem()
{
 	
	PrimaryActorTick.bCanEverTick = false;

	ChildActorComponent = CreateDefaultSubobject<UChildActorComponent>("Child Actor Component");
	BillboardComponent = CreateDefaultSubobject<UBillboardComponent>("Billboard Component");
	BillboardComponent -> SetupAttachment(ChildActorComponent);
}

void ARandomItem::BeginPlay()
{
	Super::BeginPlay();

	ChildActorComponent->SetChildActorClass(RandomItemPool[FMath::RandRange(0, RandomItemPool.Num() - 1)]);

	this->SetActorRotation(FRotator(0.0f, 90, 0.0f));
	
}


